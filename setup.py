#!/usr/bin/env python
import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

packages = ['ptimpy']

setup(name='ptimpy',
      version='0.1',
      description='Timer for puzzles.',
      author='Keith Tauscher',
      author_email='Keith.Tauscher@colorado.edu',
      packages=packages,
     )
          
PTIMPY_env = os.getenv('PTIMPY')          
cwd = os.getcwd()
          
##
# TELL PEOPLE TO SET ENVIRONMENT VARIABLE
##
if not PTIMPY_env:

    import re    
    shell = os.getenv('SHELL')

    print "\n"
    print "#"*78
    print "It would be in your best interest to set an environment variable"
    print "pointing to this directory.\n"

    if shell:    

        if re.search('bash', shell):
            print "Looks like you're using bash, so add the following to " +\
                  "your .bashrc:\n\n    export PTIMPY=%s" % cwd
        elif re.search('csh', shell):
            print "Looks like you're using csh, so add the following to " +\
                  "your .cshrc:\n\n    setenv PTIMPY %s" % cwd

    print "\nGood luck!"
    print "#"*78        
    print "\n"

# Print a warning if there's already an environment variable but it's pointing
# somewhere other than the current directory
elif DARE_env != cwd:
    print "\n"
    print "#"*78
    print "It looks like you've already got an PTIMPY environment "
    print "variable set, but it's pointing to a different directory:"
    print "\n    PTIMPY=%s" % PTIMPY_env
    print "\nHowever, we're currently in %s.\n" % cwd
    print "Is this just a typo in your environment variable?"
    print "#"*78        
    print "\n"

